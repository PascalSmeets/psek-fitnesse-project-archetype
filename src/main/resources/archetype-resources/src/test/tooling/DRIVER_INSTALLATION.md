# How to install the browser specific webdriver drivers

## Windows

### Installation of Microsoft Webdriver

1. Download the latest version from [https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)
2. Save the file in src/test/tooling/win
3. Check the file src/test/resources/selenium.properties and verify that the location and filename of the property `webdriver.edge.driver` match your downloaded file


### Installation of Mozilla Geckodriver 

1. Download the latest version from [https://github.com/mozilla/geckodriver/releases](https://github.com/mozilla/geckodriver/releases)
2. Save the file in src/test/tooling/win
3. Check the file src/test/resources/selenium.properties and verify that the location and filename of the property `webdriver.gecko.driver` match your downloaded file


### Installation of ChromeDriver 

1. Download the latest version from [https://sites.google.com/a/chromium.org/chromedriver/](https://sites.google.com/a/chromium.org/chromedriver/)
2. Save the file in src/test/tooling/win
3. Check the file src/test/resources/selenium.properties and verify that the location and filename of the property `webdriver.chrome.driver` match your downloaded file


## MacOs

### Installation of Mozilla Geckodriver 

1. Download the latest version from [https://github.com/mozilla/geckodriver/releases](https://github.com/mozilla/geckodriver/releases)
2. Save the file in src/test/tooling/macos
3. Check the file src/test/resources/selenium.properties and verify that the location and filename of the property `webdriver.macos.gecko.driver` match your downloaded file


### Installation of ChromeDriver 

1. Download the latest version from [https://sites.google.com/a/chromium.org/chromedriver/](https://sites.google.com/a/chromium.org/chromedriver/)
2. Save the file in src/test/tooling/macos
3. Check the file src/test/resources/selenium.properties and verify that the location and filename of the property `webdriver.macos.chrome.driver` match your downloaded file
