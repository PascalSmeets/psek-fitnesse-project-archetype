Only in DUTCH for now.

BEPALEN BROWSER
===============

Deze installatie van fitnesse gebruikt webdriver, dit houd in dat er gekozen moet worden
welke browser daadwerkelijk gebruikt gaat worden voor testen.

In het bestand src/tst/resources/selenium.propeties staat een sleutel:

webdriver.browser=ie

Standaard staat deze ingesteld op ie (Internet Explorer), geef hier aan welke
browser gebruikt moet worden bij het uitvoeren van de testen.

Mogelijke opties zijn:

firefox, chrome, safari, internet explorer, headless

INTERNET EXPLORER
=================

Wanneer men Internet Explorer wilt gebruiken dan dient men een aantal extra stappen te zetten.

1. In de tooling map staan de volgende twee bestanden:

   ie64.reg
   ie32.reg

   Wanneer op de computer waar de testen gedraait gaan worden een 32 bits versie van Internet
   Explorer staat geinstalleerd dan dient men bij de volgende stap de ie32.reg bestand te gebruiken.

   Wanner op de computer een 64 bits versie staat dan dient men bij de volgende stap het ie64.reg
   bestand te gebruiken.

   Om te bepalen of er een 32 of 64 bits versie van Internet Explorer op de computer staat kan men
   Internet Explorer eerst gewoon starten. Ga dan naar de Taskmanager en kijk of er bij het process
   *32 achter staat. Als dit het geval is dan gaat het om een 32 Bits versie.

   Het correcte bestand dient vanuit de verkenner gestart te worden, er zal een vraag
   verschijnen of men het bestand wilt importeren. Kies er voor om dit uit te voeren.

2. In de map src/tst/resources staat een bestand internetexplorer.properties, hier moet ook aangegeven
   worden of de 32 of 64 bits versie gebruikt wordt van Internet Explorer.

3. Zorg ervoor dat in Internet Explorer de indicatie Enable Protected Mode bij alle zone's hetzelfde is
   ingesteld. Dan kan via Internet Explorer -> Tools (Menu) -> Internet Options (Menu optie) -> Security (tabblad).

   Hier staat bij elk Zone een mogelijkheid om een vinkje op te geven. Zorg dat bij alle zone's het vinkje of
   aan of uit staat, zolang ze maar bij allemaal hetzelfde ingesteld staat.

4. Om automatisch via de aanroep van een URL in te loggen zoals in firefox (http://USERNAME:PASSWORD@url) moeten in
   de registry twee aanpassingen worden gedaan, op de locaties:

   [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_HTTP_USERNAME_PASSWORD_DISABLE]
   [HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_HTTP_USERNAME_PASSWORD_DISABLE]

   Voeg onder beide keys een DWORD toe met naam "iexplore.exe" en waarde 0 ("iexplore.exe"=dword:00000000)

STARTEN SELENIUM SERVER
=======================

Starten:

mvn clean install -P start-selenium-server

STARTEN FITNESSE WIKI
=====================

Starten:

mvn clean install -P start-fitnesse-wiki

FITNESSE WIKI OPENEN
====================

Het is raadzaam om de Fitnesse Wiki te openen via Firefox, dit heeft verder geen invloed
op het draaien van de testen want hiervoor wordt een nieuwe browser gestart.

Ga naar: http://localhost:9123 om de wiki te openen.

GENEREREN FITNESSE DOCUMENTATIE
===============================

Aan de content.txt files kan documentatie toegevoegd worden, door het volgende blok bovenaan de pagina te plaatsen:

DOCUMENTATION-BEGIN
DOCUMENTATION-TESTCASE <de naam van je testcase, bijvoorbeeld TestSuite.VoorbeeldTest>
DOCUMENTATION-LINK <een link naar jira/infoplein, bijvoorbeeld http://www.kvk.nl>
DOCUMENTATION-PURPOSE <korte omschrijving van de testcase, 1 regel>
<lange omschrijving van je testcase, je kunt hier gebruik maken van Markdown syntax (https://daringfireball.net/projects/markdown/), HTML of plain text.
Meerdere regels>
DOCUMENTATION-END

In bovenstaand blok is tussen <> aangegeven welke informatie je op welke plek kan plaatsen.

Om de documentatie vervolgens te generen kan het volgende commando gebruikt worden:

mvn gx-fitnesse-documentation:report

Zorg wel dat je in je pom.xml de map meegeeft waar de documentatie voor gegenereerd moet worden:

            <plugin>
                <groupId>nl.kvk.fitnesse</groupId>
                <artifactId>gx-fitnesse-documentation-plugin</artifactId>
                <version>${gx-fitnesse-documentation-plugin.version}</version>
                <configuration>
                    <suites>
     ----------->      <suite>TestSuite</suite>       <---------
                    </suites>
                </configuration>
            </plugin>

De documentatie zal geplaatst worden in de map target/fitnesse-documentation

PROJECT INDELING
================

Het gegenereerde project is zo opgezet dat het makkelijk moet zijn om in de toekomst een nieuwe
versie neer te zetten is. Hierdoor is er voor gekozen om een bepaalde mappen stuctuur aan te brengen
waarin er mappen zijn die je wel mag aanpassen en mappen die niet aangepast mogen worden.

Een reden voo een nieuwe versie zou kunnen zijn dat de fitnesse zelf vernieuwd is of de webdriver is vernieuwd.

Hieronder volgen de belangrijkste mappen en hun doel:

/src/test/resources/FitnesseRoot

In deze map mogen geen wijzigingen aangebracht worden, dit is een gegenereerde map die standaard bestanden bevat.

/src/test/resources/OrganisationSuite

   Deze map bevat de KvK breede Scenario's en Templates. Deze mogen niet zomaar aangepast worden. Het doel van
   deze map is er voor te zorgen dat iedereen dezelfde scenario's hanteerd voor algemene acties. Mocht men hier
   een aanpassing in willen doorvoeren dan is het raadzaam om dit in het gx-fitnesse-archetype project te doen.
   Op deze manier kunnen teams elkaar helpen met het toevoegen van nieuwe scenario's waar iedereen wat aan heeft.

   Deze scenario's mogen niet product specifiek zijn, het zullen dus veelal scenario's zijn voor algemene
   fixtures. (bijvorbeeld voor database acties, webservices, queue's etc...). Als er bijvoorbeeld een scenario
   gemaakt is om een inschrijving in het domein te veroorzaken is dan dient deze niet hier geplaatst te worden
   maar in het domein fitnesse project.

   Gaat het om een scenario die een generieke toegang bied tot een database (wat dus toepasbaar is voor
   verschillende databases) dan kan deze wel hier geplaatst worden. Maar dan natuurlijk wel toevoegen aan
   gx-fitnesse-arcetype, anders is er het risico dat het scenario weg is wanneer men een upgrade doet.

/src/test/resources/TestSuite

   Dit is de Applicatie test suite, alle fitnesse testen, scenario's en templates die voor een applicatie specifiek zijn
   dienen hier geplaatst te worden.

/src/test/resources/application.properties

   Dit is een standaard aanwezig zijnde properties file waarin applicatie specifieke properties opgenomen kunnen worden
   deze properties kunnen binnen testen gebruikt worden met de PropertiesFixture.

   Een voorbeeld hiervan is terug te vinden in de /src/test/resources/TestSuite/StartApplication test.

   Het is geen verplichting om deze te gebruiken en dient meer ter voorbeeld, het is bijvoorbeeld mogelijk om te zeggen
   dat je specifieke application.properties wilt hebben per omgeving waarin dan de omgevings specifieke variabelen opgenomen
   zijn.

/src/test/resources/internetexplorer.properties

   Zie bovenaan bij INTERNET EXPLORER om uitleg te krijgen over dit bestand.

/src/test/resources/selenium.properties

   Hierin staat welke browser gebruikt moet worden voor het uitvoeren van de fitnesse scripts.

/src/test/tooling

   Zie bovenaan bij INTERNET EXPLORER om uitleg te krijgen over de map.