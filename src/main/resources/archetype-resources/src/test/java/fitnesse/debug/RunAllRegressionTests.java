#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.fitnesse.debug;

import fitnesse.junit.FitNesseRunner;
import fitnesse.junit.FitNesseRunner.ConfigFile;
import fitnesse.junit.FitNesseRunner.FitnesseDir;
import fitnesse.junit.FitNesseRunner.OutputDir;
import fitnesse.junit.FitNesseRunner.*;
import nl.psek.fitnesse.junit.Condition;
import nl.psek.fitnesse.junit.ConditionalFitNesseRunner;
import nl.psek.fitnesse.junit.rules.SystemPropertyConditionRule;
import org.junit.runner.RunWith;

@Condition(rule = SystemPropertyConditionRule.class)
@RunWith(ConditionalFitNesseRunner.class)

// Used to specify the testsuite/testcase that should be run
@FitNesseRunner.Suite("TestSuite")

// Include ONLY test with the tag specified SuiteFilter
@FitNesseRunner.SuiteFilter("regression")
// Exclude ANY tests with the tag specified in ExcludeSuiteFilter
//@FitNesseRunner.ExcludeSuiteFilter("in progress")

@FitnesseDir(value = "src/test/resources/"
        , fitNesseRoot = "FitnesseRoot")
@OutputDir("target/fitnesse-report")
@ConfigFile("src/test/resources/plugins.properties")


// If you run from IntelliJ be sure to turn the profile 'start-regressie-test' on in the Maven Projects panel
// If you run from the commandline use 'mvn clean install test -DskipTests=false -P start-regressie-test -Dforkcount=1'
// If you run from the Jenkins use 'clean install -DskipTests=false -P start-regressie-test -DforkCount=0'
public class RunAllRegressionTests {

}