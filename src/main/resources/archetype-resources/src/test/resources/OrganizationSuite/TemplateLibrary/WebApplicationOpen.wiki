---
Help
Static
Suites
---
!3 Load selenium server properties
!*> Load selenium server properties
!|script|
|start|PropertiesFixture|selenium.properties|
|$webdriver_browser=|get string property|webdriver.browser      |
*!

!3 Load application properties
!*> Load application properties
!|script|
|start|PropertiesFixture|application.properties|
|$$application_title=|get string property|application.title|
|$$application_url=  |get string property|application.url  |
*!

!3 Open the application
!*> Open the application
!|script|
|open web application; |${seleniumServerBrowser} |${application.url} |${application.name}|
*!