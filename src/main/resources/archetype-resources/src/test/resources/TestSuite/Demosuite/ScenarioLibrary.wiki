!*> screenshot scenario's
!|scenario|take a screenshot _            |locator |
|show     |takeScreenshot;                |@locator|

!|scenario|take a screenshot of the page  |
|show     |takeScreenshot;                |
*!


!*> using different browsers
!|scenario              |use browser _  |browsername                     |
|start                  |Slim Web Driver|selenium.@browsername.properties|
|maximizeWindow;                                                         |
|defaultCommandTimeout; |$command_timeout                                |
|defaultPageLoadTimeout;|$pageload_timeout                               |

!|scenario             |check browser version                                      |
|openWebsite;          |https://whichbrowser.net                                   |
|waitForElementPresent;|//*[@id='container' and (contains(text(),'You are using'))]|
|show                  |getText;                   |id=container                   |
*!