---
Suites: demo, ibanfixture, smoke, ci
Test
---
!-<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Reminder</h3>
  </div>
  <div class="panel-body">
    The IBAN Fixture can generate and verify (11-proef) a Dutch bank account number and IBAN.<br/>
    At the moment IBAN numbers for NL and DK are supported. Other country codes may work but are not supported.
  </div>
</div>-!
#
#
!|script             |
|start|Iban Generator|
#
#
!3 !-<span class="label label-primary">Generating an accountnumber | Genereren van een rekeningnummer</span>-!
#
!|script                                |
|show            |generateAccountNumber;|
|show            |generateAccountNumber;|
|$accountnumber1=|generateAccountNumber;|
|$accountnumber2=|generateAccountNumber;|
#
#
!3 !-<span class="label label-primary">Generating an IBAN | Genereren van een IBAN</span>-!
#
#
Using countrycode and bankcode | Met landcode en bankcode
#
!|script                      |
|$iban1=|generateIBAN;|NL|RABO|
|$iban2=|generateIBAN;|NL|SNSB|
|show   |generateIBAN;|NL|XXXX|
|show   |generateIBAN;|NL|KVKT|
#
Using countrycode, bankcode and accountnumber  | Met landcode, bankcode en rekeningnummer
#
!|script                                      |
|$iban3=|generateIBAN;|NL|RABO|$accountnumber1|
|$iban4=|generateIBAN;|NL|SNSB|$accountnumber2|
#
#
!3 !-<span class="label label-primary">Validating an IBAN | Genereren van een IBAN</span>-!
#
#
This does not check for an existing bankcode. | Hierbij wordt niet gecontroleerd of de bankcode bestaat.
!|script                              |
|validateDutchIban;|$iban1            |
|validateDutchIban;|$iban2            |
|validateDutchIban;|$iban3            |
|validateDutchIban;|$iban4            |
|validateDutchIban;|NL72XXXX0457104538|
