---
Static
Test: no
---
# Verwijder het comment teken '#' bij de omgeving die je wilt gebruiken,
# voeg het toe bij de omgeving die je niet wilt gebruiken en
# klik daarna op save.

#!define environment {acc}
#!define environment {test}
!define environment {demo}

