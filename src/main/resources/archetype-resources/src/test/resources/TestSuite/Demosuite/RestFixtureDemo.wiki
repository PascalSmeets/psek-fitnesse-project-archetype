---
Suites: demo, restfixture
Test
---
!|script                                          |
|start             |Rest Fixture                  |
|enablelogMessagesToSystemOut;                    |
|setBaseUri;       |https://google.com            |
|addQueryParameter;|q                     |psek.nl|
|sendRequest;      |get                   |/      |
|check             |getResponseStatusCode;|200    |
