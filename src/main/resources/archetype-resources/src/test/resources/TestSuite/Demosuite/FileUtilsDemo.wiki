---
Suites: demo, datefixture, smoke
Test
---
!|script                                     |
|start            |FileUtils                 |
|verifyFileExists;|pom.xml                   |
|show             |getFileSize;     |pom.xml |
|reject           |verifyFileExists;|pom.yml |
|reject           |verifyFileExists;|/pom.xml|
