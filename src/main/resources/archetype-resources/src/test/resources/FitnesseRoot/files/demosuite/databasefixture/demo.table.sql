CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255),
    Remarks CLOB(1024)
);

insert into Persons
    values(1, 'Jones', 'Jack', 'Beech lane 12', 'Greenville','This is the CLOB string for Jack');

insert into Persons
    values(2, 'Graham', 'Grace', 'Birch road 5', 'Greenville','This is the CLOB string for Grace');

insert into Persons
    values(3, 'Franks', 'Fred', 'Alder 2', 'Greenville','This is the CLOB string for Fred');

insert into Persons
    values(4, 'Lane', 'Lois', 'Alder 78A', 'Greenville','This is the CLOB string for Lois');
